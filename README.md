A method of showing the connection status of ProtonVPN with [Waybar](https://github.com/Alexays/Waybar) for [Sway](https://github.com/swaywm/sway) window manager.

Dependencies:
    [protonvpn-cli-ng](https://github.com/ProtonVPN/protonvpn-cli-ng)
    
    
The ProtonVPN logo is owned by [Proton Technologies AG](https://protonvpn.com/press). 