#!/usr/bin/env python
import sys
import json
import subprocess

bashProton = "protonvpn s"
protonInfo = subprocess.Popen(bashProton.split(), stdout=subprocess.PIPE)

def writeOutput(connectionInfo):
    successCond = {'text': '   ', 'tooltip': connectionInfo, 'class' : 'proton' }
    sys.stdout.write(json.dumps(successCond) + '\n')
    sys.stdout.flush()

def main():	
    (output, err) = protonInfo.communicate()
    protonResp = output.decode('utf_8')

    if "Connected" in protonResp:
	    writeOutput(protonResp)

if __name__ == '__main__':
    main()
